package com.mobiquityinc.packer;

import com.mobiquityinc.exception.*;
import java.util.Collection;

public class Packer {

    public static String pack(String inputFilePath) throws APIException {

        InputReader inputReader = new InputReader();
        Collection<PackageContainer> packageContainers = inputReader.readFile(inputFilePath);

        StringBuilder builder = new StringBuilder();
        for(PackageContainer packageContainer : packageContainers) {
            builder.append(packageContainer.computeOptimalItems());
            builder.append("\n");
        }
        return builder.toString();
    }
}
