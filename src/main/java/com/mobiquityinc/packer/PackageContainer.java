package com.mobiquityinc.packer;

import java.util.*;

public class PackageContainer {

    private ArrayList<PackageItem> packages = new ArrayList<>();
    private int packageLimit;

    public void setLimit(int limit) {
        packageLimit = limit;
    }
    public void addItem(PackageItem item) {
        packages.add(item);
    }


    /**
     * selects a subset of the package items added so that the total weight of the selected items
     * is less than or equal to the package limit and the cost is maximized
     * a subset with a minimal weight is selected in case of two or more ties has a maximum cost
     * the runtime and space complexity of the algorithm is O(limit * itemsCount * SCALE_FACTOR)
     * returns the computed packages' indexes as a comma separated list or "-" if no items selected
     */
    public String computeOptimalItems() {
        List<Integer> result = selectOptimalItems();
        Collections.sort(result);

        if(result.size() == 0)
            return "-";
        
        StringBuilder builder = new StringBuilder();
        builder.append(result.get(0));
        for(int i=1; i<result.size(); i++)
            builder.append(", " + result.get(i));
        return builder.toString();
    }


    private List<Integer> selectOptimalItems() {

        int scaledPackageLimit = packageLimit * PackageItem.SCALE_FACTOR;

        // each item in the dp array (index i, j) should contain the maximum cost possible
        // using i+1 items with weight limit j
        int[][] dp = new int[packages.size()+1][scaledPackageLimit+1];
        solveForSubProblems(dp, scaledPackageLimit);
        return identifySelectedItems(dp, scaledPackageLimit);
    }


    private void solveForSubProblems(int[][] dp, int scaledPackageLimit) {

        for(int currentWeight=1; currentWeight<=scaledPackageLimit; currentWeight++) {
            for(int currentItem=1; currentItem<=packages.size(); currentItem++) {
                dp[currentItem][currentWeight] = dp[currentItem-1][currentWeight];
                if(currentWeight >= packages.get(currentItem-1).getScaledWeight()) {
                    int possibleSolution = dp[currentItem-1][currentWeight-packages.get(currentItem-1).getScaledWeight()] + (int)packages.get(currentItem-1).getCost();
                    dp[currentItem][currentWeight] = Math.max(dp[currentItem][currentWeight], possibleSolution);
                }
            }
        }
    }



    private ArrayList<Integer> identifySelectedItems(int[][] dp, int scaledPackageLimit) {
        ArrayList<Integer> path = new ArrayList<>();
        ArrayList<Integer> optimalSelection = new ArrayList<>();
        identifySelectedItems(dp, path, optimalSelection, packages.size(), scaledPackageLimit);
        return optimalSelection;
    }


    /**
     * performing a depth first search on the DP array to figure out the possible routs that
     * resulted in the final solution
     */
    private void identifySelectedItems(int[][] dp, ArrayList<Integer> curPath, ArrayList<Integer> optimalSelection, int x, int y) {

        if(dp[x][y] == 0) {
            if(optimalSelection.size() == 0 || calculateWeight(curPath) < calculateWeight(optimalSelection)) {
                optimalSelection.clear();
                optimalSelection.addAll(curPath);
            }
            return;
        }

        // dfs in sub nodes
        if(dp[x][y] == dp[x-1][y])
            identifySelectedItems(dp, curPath, optimalSelection, x-1, y);

        if(y >= packages.get(x-1).getScaledWeight() && dp[x][y] == dp[x-1][y-packages.get(x-1).getScaledWeight()] + packages.get(x-1).getCost()) {
            curPath.add(x);
            identifySelectedItems(dp, curPath, optimalSelection, x - 1, y - packages.get(x - 1).getScaledWeight());
            curPath.remove(curPath.size()-1);
        }
    }

    private int calculateWeight(ArrayList<Integer> curPath) {
        int totalWeight = 0;
        for(Integer item : curPath)
            totalWeight += packages.get(item-1).getScaledWeight();
        return totalWeight;
    }


    @Override
    public int hashCode() {
        // equal objects must have equal packageLimit and has to return the same hash code
        return packageLimit;
    }

    @Override
    public boolean equals(Object obj) {
        PackageContainer other = (PackageContainer)obj;
        if( packageLimit != other.packageLimit || packages.size() != other.packages.size())
            return false;
        for(int i=0; i<packages.size(); i++)
            if(!packages.get(i).equals(other.packages.get(i)))
                return false;
        return true;
    }
}
