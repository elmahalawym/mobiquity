package com.mobiquityinc.packer;

/**
 *
 */
public class PackageItem {

    // scale factor is used to scale the weight parameter to be able
    // to use the weight as an integer parameter, the value 10 takes in consideration one decimal point
    // when calculating the optimal packages
    // the larger the factor is, the more memory and time needed for the algorithm to run
    public static final int SCALE_FACTOR = 10;
    private int index;
    private double weight;
    private double cost;
    private int scaledWeight;

    /**
     * Contructe pakage tiotem
     * @param index
     * @param weight
     * @param cost
     */
    public PackageItem(int index, double weight, double cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
        this.scaledWeight = (int)(weight * SCALE_FACTOR);
    }


    public double getCost() {
        return cost;
    }

    public int getScaledWeight() {
        return scaledWeight;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        PackageItem other = (PackageItem)obj;
        return index == other.index &&
                weight == other.weight &&
                cost == other.cost;
    }
}
