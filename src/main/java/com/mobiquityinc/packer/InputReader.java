package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class InputReader {

    public Collection<PackageContainer> readFile(String filePath) throws APIException {

        if(filePath == null || filePath.isEmpty())
            throw new APIException("Invalid file path");

        File file = new File(filePath);
        if(!file.exists() || !file.isFile())
            throw new APIException("input file doesn't exist");

        Collection<PackageContainer> packageContainers = new ArrayList<>();
        try {
            try(BufferedReader br = new BufferedReader(new FileReader(file))) {
                for(String line; (line = br.readLine()) != null; ) {
                    packageContainers.add(processLine(line));
                }
            }
        } catch(IOException ex) {
            throw new APIException("Error while reading input file!", ex);
        }

        return packageContainers;
    }


    private PackageContainer processLine(String line) {

        PackageContainer result = new PackageContainer();
        String[] segments = line.split(":");
        result.setLimit(Integer.parseInt(segments[0].trim()));

        String[] itemSegments = segments[1].trim().split(" ");
        for (String itemAsText: itemSegments) {

            if(itemAsText.trim().isEmpty())
                continue;
            result.addItem(createItem(itemAsText));
        }

        return result;
    }

    private PackageItem createItem(String itemAsText) {
        String[] segments = itemAsText.substring(1, itemAsText.length()-1).split(",");
        return new PackageItem(Integer.parseInt(segments[0].trim()), Double.parseDouble(segments[1].trim()), Double.parseDouble(segments[2].trim().substring(1)));
    }

}
