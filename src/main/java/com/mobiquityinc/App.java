package com.mobiquityinc;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.Packer;

public class App 
{
    public static void main( String[] args ) {

        if(args.length != 1) {
            System.out.println("usage: app.exe <input_file_path>");
            return;
        }

        try {
            System.out.println(Packer.pack(args[0]));
        } catch(APIException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
