package com.mobiquityinc;

import com.mobiquityinc.packer.PackageContainer;
import com.mobiquityinc.packer.PackageItem;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PackageContainerTest {

    @Test
    public void emptyPackageShouldResultInEmptySelection() {
        PackageContainer container = new PackageContainer();
        container.setLimit(10);
        assertEquals("-", container.computeOptimalItems());
    }

    @Test
    public void containerWithOnePackageLessThanOrEqualLimitShouldBeSelected() {
        PackageContainer container = new PackageContainer();
        container.setLimit(10);
        container.addItem(new PackageItem(1, 10, 100));
        assertEquals("1", container.computeOptimalItems());
    }

    @Test
    public void noItemsShouldBeSelectedIfAllWeightsGreatherThanLimit() {
        PackageContainer container = new PackageContainer();
        container.setLimit(10);
        container.addItem(new PackageItem(1, 11, 100));
        container.addItem(new PackageItem(2, 12, 100));
        assertEquals("-", container.computeOptimalItems());
    }


}
