package com.mobiquityinc;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.InputReader;
import com.mobiquityinc.packer.PackageContainer;
import com.mobiquityinc.packer.PackageItem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InputReaderTest {
    private static final String INPUT_FILE_PATH = "input.txt";

    @Test
    public void inputReaderTest() throws APIException {
        InputReader inputReader = new InputReader();
        Collection<PackageContainer> packagesCollection = inputReader.readFile(INPUT_FILE_PATH);
        List<PackageContainer> packages = new ArrayList(packagesCollection);
        assertEquals(4, packages.size());

        PackageContainer first = new PackageContainer();
        first.setLimit(81);
        first.addItem(new PackageItem(1,53.38,45));
        first.addItem(new PackageItem(2,88.62,98));
        first.addItem(new PackageItem(3,78.48,3) );
        first.addItem(new PackageItem(4,72.30,76) );
        first.addItem(new PackageItem(5,30.18,9) );
        first.addItem(new PackageItem(6,46.34,48));

        assertEquals(first, packages.get(0));

        PackageContainer second = new PackageContainer();
        second.setLimit(8);
        second.addItem(new PackageItem(1,15.3,34));

        assertEquals(second, packages.get(1));
    }
}
