package com.mobiquityinc;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.PackageContainer;
import com.mobiquityinc.packer.PackageItem;
import com.mobiquityinc.packer.Packer;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Unit tests.
 */
public class AppTest 
{
    private static final String INPUT_FILE_PATH = "input.txt";

    @Test
    public void testRun() throws APIException {
        String result = Packer.pack(INPUT_FILE_PATH);
        String expected = "4\n";
        expected += "-\n";
        expected += "2, 7\n";
        expected += "8, 9\n";
        assertEquals(expected, result);
    }
}
